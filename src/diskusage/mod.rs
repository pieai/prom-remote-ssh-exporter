use log::{error};
use ssh2::Session;
use crate::ssh::exec;

pub fn du(_session: &Session, _dir: &str) -> u32
{
    let command = format!("du -s {}", _dir);
    //println!("command: {}", command);
    let output = match exec(&command, &_session)
    {
        Err(e) => {
            error!("could not exec command: {:?}", e);
            return 0;
        }
        Ok(o) => o
    };
    if output.len() == 0
    {
        return 0;
    }

    //println!("output: {}", output);
    let mut parts = output.split_whitespace();
    let size = parts.next().unwrap();
    return size.parse::<u32>().unwrap();
}


#[derive(Debug, Default)]
pub struct UsageEntry {
    pub folder: String,
    pub size: u32,
}

#[derive(Debug, Default)]
pub struct Usage {
    pub attributes: Vec<UsageEntry>,
}

pub fn usage(_session: &Session, _basedir: &str) -> Usage
{
    // basedir = /srv/mail/mysql/data
    // ls -ld /srv/mail/mysql/data/*/
    // find /srv/mail/mysql/data -type d -depth 1
    // _basedir
    let command = format!("ls -1d {}/*/", _basedir);
    let inner_folders = match exec(&command, &_session)
    {
        Ok(folders) => folders,
        Err(e) => {
            error!("could not check usage: {:?}", e);
            let usages: Vec<UsageEntry> = Vec::new();
            return Usage {
                attributes: usages
            };
        }
    };
    //println!("inner_folders:\n====\n{}====", inner_folders);

    let mut usages: Vec<UsageEntry> = Vec::new();
    for folder in inner_folders.lines() {
        let du = du(_session, folder);
        //println!("folder: {} ", folder);
        //println!("diskusage: {} ", du);
        let mut f = folder.to_string();
        if f.ends_with("/") { f.pop(); };
        usages.push(UsageEntry { folder: f, size: du });
    }

    let du = du(_session, _basedir);
    //println!("folder: {} ", _basedir);
    //println!("diskusage: {} ", du);
    usages.push(UsageEntry { folder: _basedir.to_string(), size: du });
    let res = Usage {
        attributes: usages
    };
    return res;
}




// --output: FIELD_LIST  is  a  comma-separated  list of columns to be included.  Valid field names are: 'source', 'fstype', 'itotal', 'iused',
//        'iavail', 'ipcent', 'size', 'used', 'avail', 'pcent', 'file' and 'target' (see info page).

// df -h -t ext4 -t vfat -l --output=size,used,avail,target
// df -t ext4 -t vfat -l --output=size,used,avail,target
/*

1K-Blöcke Benutzt    Verf. Eingehängt auf
 29292924 5002084 22779800 /
    64366   21530    42836 /boot

 */
