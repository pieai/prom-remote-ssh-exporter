extern crate ssh_prometheus_exporter;

use std::env;
use std::fs::File;
use std::io::BufReader;

use log::{info, trace};
use log::{debug, error};
use prometheus_exporter_base::{MetricType, PrometheusMetric, render_prometheus};
use serde::Deserialize;
use ssh2::Session;

use ssh_prometheus_exporter::{iostats, ssh};
use ssh_prometheus_exporter::diskusage::usage;
use ssh_prometheus_exporter::iostats::{IOStatDisk, IOStats};
use ssh_prometheus_exporter::load::{cpuinfo, LoadAverage, loadavg};
use ssh_prometheus_exporter::meminfo::meminfo;

#[derive(Debug, Clone, Default)]
struct MyOptions {}

// https://github.com/BurntSushi/rust-csv/blob/master/examples/cookbook-read-serde.rs
// By default, struct field names are deserialized based on the position of
// a corresponding field in the CSV data's header record.
#[derive(Debug, Deserialize, Default)]
struct Endpoint {
    identifier: String,
    hostname: String,
    port: i32,
    username: String,
    password: String,
    usage: String,
}

struct Args {
    help: bool,
    verbose: bool,
    endpoints: String,
    port: u16,
}

// https://www.dev-notes.eu/2020/03/Return-a-Result-Type-from-the-Main-Function-in-Rust/
#[tokio::main]
async fn main() -> Result<(), &'static str> {
    let mut args = pico_args::Arguments::from_env();
    let args = Args {
        help: args.contains(["-h", "--help"]),
        verbose: args.contains(["-v", "--verbose"]),
        endpoints: if let Ok(ep) = args.value_from_str("--endpoints") {
            ep
        } else { String::new() },
        port: 6660,
    };

    if args.verbose
    {
        env::set_var("RUST_LOG", "trace");
    } else {
        env::set_var("RUST_LOG", "info");
    }

    if args.help
    {
        println!("usage: ");
        println!("\t-h | --help show this help");
        println!("\t-v | --verbose verbose logging (trace instead of info)");
        println!("\t--endpoints enpoints csv file to use (columns: identifier,hostname,port,username,password,usage)");
        println!("\t\tidentifier: host identifier");
        println!("\t\thostname: host name or ip to connect to");
        println!("\t\tport: ssh port, 22 is the default");
        println!("\t\tusername: ssh user");
        println!("\t\tpassword: password");
        println!("\t\tusage: fs-usage directories seperated by pipe |: /var/log|/var/lib/|/home");
        return Ok(());
    }


    env_logger::init();

    let addr = ([0, 0, 0, 0], args.port).into();
    info!("starting exporter on {}", addr);
    let endpoints = args.endpoints.clone();

    render_prometheus(addr, MyOptions::default(), move |request, options| {
        async move {
            trace!(
                "in our render_prometheus(request == {:?}, options == {:?})",
                request,
                options
            );
            /*
                        if ! Path::new(endpoints.as_str()).exists()
                        {
                            Ok("# could not open endpoints file");
                        }
            */
            let input = File::open(endpoints.as_str()).unwrap();
            let buffered = BufReader::new(input);
            let mut rdr = csv::Reader::from_reader(buffered);

            let connectable_metric = PrometheusMetric::new("connectable", MetricType::Gauge, "system is connectable using ssh");
            let mut connectable_buf = connectable_metric.render_header();

            let load_1_metric = PrometheusMetric::new("node_load1", MetricType::Gauge, "system load 1 minute");
            let mut load_1_buf = load_1_metric.render_header();

            let load_5_metric = PrometheusMetric::new("node_load5", MetricType::Gauge, "system load 5 minute");
            let mut load_5_buf = load_5_metric.render_header();

            let load_15_metric = PrometheusMetric::new("node_load15", MetricType::Gauge, "system load 15 minute");
            let mut load_15_buf = load_15_metric.render_header();

            let usage_metric = PrometheusMetric::new("filesystem", MetricType::Gauge, "fs usage");
            let mut usage_buf = usage_metric.render_header();

            let memory_metric = PrometheusMetric::new("memory", MetricType::Gauge, "memory usage");
            let mut memory_buf = memory_metric.render_header();

            let machine_cpu_threads_metric = PrometheusMetric::new("machine_cpu_threads", MetricType::Gauge, "cpu thread count");
            let mut machine_cpu_threads_buf = machine_cpu_threads_metric.render_header();

            let iostats_metric = PrometheusMetric::new("iostats", MetricType::Gauge, "io stats");
            let mut iostats_metric_buf = iostats_metric.render_header();


            for entry in rdr.deserialize() {
                let record: Endpoint = entry?;
                let mut attributes: Vec<(&str, &str)> = Vec::new();
                if !record.identifier.starts_with("#")
                {
                    attributes.push(("host", &record.identifier));
                    debug!("connecting to {} via ssh", &record.hostname);

                    let sess: Session = match ssh::connect(&record.hostname, &record.port, &record.username, &record.password)
                    {
                        Ok(s) =>
                            {
                                connectable_buf.push_str(&*connectable_metric.render_sample(Some(attributes.as_slice()), 1));
                                s
                            }
                        Err(e) =>
                            {
                                error!("could not connect: {:?}", e);
                                connectable_buf.push_str(&*connectable_metric.render_sample(Some(attributes.as_slice()), 0));
                                continue;
                            }
                    };


                    //attributes.push(("instance", &record.identifier));

                    // load
                    let load: LoadAverage = loadavg(&sess);
                    load_1_buf.push_str(&load_1_metric.render_sample(Some(attributes.as_slice()), load.load1));
                    load_5_buf.push_str(&load_5_metric.render_sample(Some(attributes.as_slice()), load.load5));
                    load_15_buf.push_str(&load_15_metric.render_sample(Some(attributes.as_slice()), load.load15));

                    // cpuinfo
                    let cpuinfo = cpuinfo(&sess);
                    machine_cpu_threads_buf.push_str(&machine_cpu_threads_metric.render_sample(Some(attributes.as_slice()), cpuinfo.threads));

                    if !record.usage.trim().is_empty()
                    {
                        for u in record.usage.split("|")
                        {
                            let usage = usage(&sess, &u);

                            for entry in usage.attributes
                            {
                                let mut usage_attributes: Vec<(&str, &str)> = Vec::new();
                                usage_attributes.push(("host", &record.identifier));
                                usage_attributes.push(("folder", &entry.folder));
                                usage_buf.push_str(&usage_metric.render_sample(Some(usage_attributes.as_slice()), entry.size));
                            }
                        }
                    }

                    // meminfo
                    let meminfo = meminfo(&sess);
                    for entry in meminfo.attributes
                    {
                        let mut memory_attributes: Vec<(&str, &str)> = Vec::new();
                        memory_attributes.push(("type", &entry.name));
                        memory_attributes.push(("host", &record.identifier));
                        memory_buf.push_str(&memory_metric.render_sample(Some(memory_attributes.as_slice()), entry.size));
                    }

                    // iostat
                    let iostats = iostats::iostats(&sess);
                    match iostats {
                        Ok(iostats) => {
                            render_iostats(&iostats_metric, &mut iostats_metric_buf, &record, &iostats);
                        }
                        _ => {}
                    }
                }
            }


            let mut s = connectable_buf;
            s.push_str(&load_1_buf);
            s.push_str(&load_5_buf);
            s.push_str(&load_15_buf);
            s.push_str(&usage_buf);
            s.push_str(&memory_buf);
            s.push_str(&machine_cpu_threads_buf);
            s.push_str(&iostats_metric_buf);

            Ok(s)
        }
    })
        .await;
    Ok(())
}

fn render_iostats(iostats_metric: &PrometheusMetric, mut iostats_metric_buf: &mut String, record: &Endpoint, iostats: &IOStats) {
    if iostats.uptime.valid
    {
        for disk in &iostats.uptime.disks
        {
            render_iostat_metrics_for_disk(&iostats_metric, &mut iostats_metric_buf, &record, &disk, "uptime");
        }
    }
    if iostats.moment.valid
    {
        for disk in &iostats.uptime.disks
        {
            render_iostat_metrics_for_disk(&iostats_metric, &mut iostats_metric_buf, &record, &disk, "moment");
        }
    }
}

fn render_iostat_metrics_for_disk(iostats_metric: &PrometheusMetric, iostats_metric_buf: &mut String, record: &Endpoint, disk: &IOStatDisk, iostat_type: &str) {
    // render names metrics
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_read_completed_per_second", disk.requests_read_completed_per_second);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_read_kilobytes_per_second", disk.requests_read_kilobytes_per_second);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "queued_requests_read_completed_per_second", disk.queued_requests_read_completed_per_second);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_read_merged_percentage", disk.requests_read_merged_percentage);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_read_time_average_milliseconds", disk.requests_read_time_average_milliseconds);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_read_size_average_kilobyte", disk.requests_read_size_average_kilobyte);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_write_completed_per_second", disk.requests_write_completed_per_second);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_write_kilobytes_per_second", disk.requests_write_kilobytes_per_second);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "queued_requests_writed_completed_per_second", disk.queued_requests_writed_completed_per_second);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_write_merged_percentage", disk.requests_write_merged_percentage);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_write_time_average_milliseconds", disk.requests_write_time_average_milliseconds);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_write_size_average_kilobytes", disk.requests_write_size_average_kilobytes);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_discarded_completed_per_second", disk.requests_discarded_completed_per_second);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_discarded_kilobytes_per_second", disk.requests_discarded_kilobytes_per_second);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "queued_requests_discarded_completed_per_second", disk.queued_requests_discarded_completed_per_second);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_discarded_merged_percentage", disk.requests_discarded_merged_percentage);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_discarded_time_average_milliseconds", disk.requests_discarded_time_average_milliseconds);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_discarded_size_average_kilobytes", disk.requests_discarded_size_average_kilobytes);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "requests_discarded_queue_length_average", disk.requests_discarded_queue_length_average);
    render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, "saturation", disk.saturation);

    // render unnamed metrics
    for raw_metric in &disk.metrics
    {
        render_iostat_metric_for_disk(iostats_metric, iostats_metric_buf, &record.identifier, &disk.device, iostat_type, &raw_metric.name, raw_metric.value);
    }
}

fn render_iostat_metric_for_disk(iostats_metric: &PrometheusMetric, iostats_metric_buf: &mut String, host: &str, disk: &str, iostat_type: &str, metric: &str, value: f32) {
    let mut memory_attributes: Vec<(&str, &str)> = Vec::new();
    memory_attributes.push(("host", host));
    memory_attributes.push(("disk", disk));
    memory_attributes.push(("type", iostat_type));
    memory_attributes.push(("metric", metric));
    iostats_metric_buf.push_str(&iostats_metric.render_sample(Some(memory_attributes.as_slice()), value));
}
