extern crate ssh2;

use ssh2::Session;
use std::io::{Read};
use std::net::{TcpStream, ToSocketAddrs};
use log::{debug, error};
use std::error::Error;
use std::time::Duration;

pub fn connect(_hostname: &str, _port: &i32, _username: &str, _password: &str) -> Result<Session, Box<dyn Error>> {
    let addr = format!("{}:{}", _hostname, _port.to_string());
    let mut socket_addr = match addr.to_socket_addrs() {
        Ok(sa) => sa,
        Err(e) => {
            error!("could not parse seocket address: {:?}", e);
            return Err(Box::new(e) as Box<dyn std::error::Error>);
        }
    };

    let tcp = match TcpStream::connect_timeout(&socket_addr.next().unwrap(), Duration::from_secs(10))
    {
        Ok(c) => c,
        Err(e) => {
            error!("could not connect: {:?}", e);
            return Err(Box::new(e) as Box<dyn std::error::Error>);
        }
    };


    let mut sess = match Session::new()
    {
        Ok(s) => s,
        Err(e) => {
            error!("could not create session: {:?}", e);
            return Err(Box::new(e) as Box<dyn std::error::Error>);
        }
    };

    debug!("connecting to: {}", _hostname);
    sess.set_tcp_stream(tcp);

    match sess.handshake()
    {
        Ok(s) => s,
        Err(e) => {
            error!("handshake failed: {:?}", e);
            return Err(Box::new(e) as Box<dyn std::error::Error>);
        }
    }

    if _password.trim().is_empty()
    {
        // use agent
        match sess.userauth_agent(_username)
        {
            Ok(l) => l,
            Err(e) => {
                error!("user auth using agent failed: {:?}", e);
                return Err(Box::new(e) as Box<dyn std::error::Error>);
            }
        }
    } else {
        match sess.userauth_password(_username, _password) {
            Ok(l) => l,
            Err(e) => {
                error!("user auth using password failed: {:?}", e);
                return Err(Box::new(e) as Box<dyn std::error::Error>);
            }
        }
    }

    if sess.authenticated()
    {
        return Ok(sess);
    } else {
        return Err(Box::from("not authenticated"));
    }
}

pub fn exec(_command: &str, _session: &Session) -> Result<String, Box<dyn Error>> {
    let mut channel = match _session.channel_session()
    {
        Ok(c) => c,
        Err(e) => {
            error!("could not get channel: {:?}", e);
            return Err(Box::new(e) as Box<dyn std::error::Error>);
        }
    };
    match channel.exec(_command)
    {
        Err(e) => {
            error!("could not exec command: {:?}", e);
            return Err(Box::new(e) as Box<dyn std::error::Error>);
        }
        _ => {}
    }
    let mut s = String::new();
    match channel.read_to_string(&mut s)
    {
        Err(e) => {
            error!("could not read response: {:?}", e);
            return Err(Box::new(e) as Box<dyn std::error::Error>);
        }
        _ => {}
    }
    match channel.wait_close()
    {
        Err(e) => {
            error!("could not close channel: {:?}", e);
            return Err(Box::new(e) as Box<dyn std::error::Error>);
        }
        _ => {}
    }
    return Ok(s);
}