use log::{error};
use ssh2::Session;
use crate::ssh::exec;

#[derive(Debug, Default)]
pub struct MemoryEntry {
    pub name: String,
    pub size: u64,
}

#[derive(Debug, Default)]
pub struct Memory {
    pub attributes: Vec<MemoryEntry>,
}

pub fn meminfo(_session: &Session) -> Memory
{
    let meminfo = match exec("cat /proc/meminfo", &_session)
    {
        Ok(meminfo) => meminfo,
        Err(e) => {
            error!("could not check memusage: {:?}", e);
            let usages: Vec<MemoryEntry> = Vec::new();
            return Memory { attributes: usages };
        }
    };
    //println!("meminfo:\n====\n{}====", meminfo);
    // MemTotal:        8002772 kB
    // MemFree:          422012 kB
    // MemAvailable:    6554824 kB
    // Buffers:          202756 kB
    // Cached:          5468388 kB
    // SwapCached:            8 kB

    let mut usages: Vec<MemoryEntry> = Vec::new();
    for line in meminfo.lines()
    {
        let mut parts = line.split_whitespace();
        // label: 'MemAvailable:'
        let mut label = parts.next().unwrap();
        // label: 'MemAvailable'
        label = &label[..label.len() - 1];
        let size = parts.next().unwrap();
        //println!("{:?}: {:?}", label, size);

        usages.push(MemoryEntry {
            name: String::from(label),
            size: size.parse::<u64>().unwrap(),
        });
    }
    return Memory { attributes: usages };
}