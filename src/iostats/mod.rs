use log::{debug, warn, error};
use ssh2::Session;
use std::error::Error;
use crate::ssh::exec;
use json::JsonValue;

#[derive(Debug, Default)]
pub struct IOStats {
    pub uptime: IOStat,
    pub moment: IOStat,
}

#[derive(Debug, Default)]
pub struct IOStat {
    // if it is valid or a dummy response
    pub valid: bool,

    // stats per disk
    pub disks: Vec<IOStatDisk>,
}

#[derive(Debug, Default)]
pub struct RawMetric {
    pub name: String,
    pub value: f32,
}

#[derive(Debug, Default)]
pub struct IOStatDisk {
    // disk_device: sda
    pub device: Box<str>,

    // raw metrics
    pub metrics: Vec<RawMetric>,

    // r/s: The number (after merges) of read requests completed per second for the device.
    pub requests_read_completed_per_second: f32,

    // rkB/s: The number of sectors (kilobytes, megabytes) read from the device per second.
    pub requests_read_kilobytes_per_second: f32,

    // rrqm/s: The number of read requests merged per second that were queued to the device.
    pub queued_requests_read_completed_per_second: f32,

    // %rrqm: The percentage of read requests merged together before being sent to the device.
    pub requests_read_merged_percentage: f32,

    // r_await: The average time (in milliseconds) for read requests issued to the device to be served.
    // This includes the time spent by the requests in queue and the time spent servicing them.
    pub requests_read_time_average_milliseconds: f32,

    // rareq-sz: The average size (in kilobytes) of the read requests that were issued to the device.
    pub requests_read_size_average_kilobyte: f32,

    // w/s: The number (after merges) of write requests completed per second for the device.
    pub requests_write_completed_per_second: f32,

    // wkB/s: The number of sectors (kilobytes, megabytes) written to the device per second
    pub requests_write_kilobytes_per_second: f32,

    // wrqm/s: The number of write requests merged per second that were queued to the device.
    pub queued_requests_writed_completed_per_second: f32,

    // %wrqm: The percentage of write requests merged together before being sent to the device.
    pub requests_write_merged_percentage: f32,

    // w_await: The average time (in milliseconds) for write requests issued to the device to be served.
    // This includes the time spent by the requests in queue and the time spent servicing them
    pub requests_write_time_average_milliseconds: f32,

    // wareq-sz: The average size (in kilobytes) of the write requests that were issued to the device.
    pub requests_write_size_average_kilobytes: f32,

    // d/s: The number (after merges) of discard requests completed per second for the device.
    pub requests_discarded_completed_per_second: f32,

    // dkB/s: The number of sectors (kilobytes, megabytes) discarded for the device per second.
    pub requests_discarded_kilobytes_per_second: f32,

    // drqm/s: The number of discard requests merged per second that were queued to the device.
    pub queued_requests_discarded_completed_per_second: f32,

    // %drqm: The percentage of discard requests merged together before being sent to the device.
    pub requests_discarded_merged_percentage: f32,

    // d_await:  The average time (in milliseconds) for discard requests issued to the device to be
    // served. This includes the time spent by the requests in queue and the time spent servicing them.
    pub requests_discarded_time_average_milliseconds: f32,

    // dareq-sz: The average size (in kilobytes) of the discard requests that were issued to the device.
    pub requests_discarded_size_average_kilobytes: f32,

    // aqu-sz: The average queue length of the requests that were issued to the device. Note: In previous
    // versions, this field was known as avgqu-sz.
    pub requests_discarded_queue_length_average: f32,

    // %util: Percentage of elapsed time during which I/O requests were issued to the device (bandwidth
    // utilization for the device). Device saturation occurs when this value is close to 100% for
    // devices serving requests serially.
    // But for devices serving requests in parallel, such as RAID arrays and modern SSDs, this number
    // does not reflect their performance limits.
    pub saturation: f32,
}

pub fn iostats(_session: &Session) -> Result<IOStats, Box<dyn Error>>
{
    // network fs stats: /proc/self/mountstats

    // https://www.thomas-krenn.com/de/wiki/Linux_I/O_Performance_Messungen_mit_iostat

    // -d: Display the device utilization report.
    // -k: Display statistics in kilobytes per second.
    // -o JSON: Display the statistics in JSON (Javascript Object Notation) format.  JSON output
    // field order is undefined, and new fields may be added in the future.

    /*
    {"sysstat": {
        "hosts": [
            {
                "nodename": "XXX",
                "sysname": "Linux",
                "release": "5.XXX",
                "machine": "x86_64",
                "number-of-cpus": 4,
                "date": "XX.XX.XXXX",
                "statistics": [
                    {
                        "avg-cpu":  {"user": 1.19, "nice": 0.00, "system": 0.35, "iowait": 0.02, "steal": 0.00, "idle": 98.43},
                        "disk": [
                            {"disk_device": "sda", "r/s": 0.08, "w/s": 2.84, "d/s": 0.00, "rkB/s": 4.66, "wkB/s": 38.12, "dkB/s": 0.00, "rrqm/s": 0.03, "wrqm/s": 0.64, "drqm/s": 0.00, "rrqm": 30.86, "wrqm": 18.45, "drqm": 0.00, "r_await": 2.03, "w_await": 1.59, "d_await": 0.00, "rareq-sz": 30.53, "wareq-sz": 6.72, "dareq-sz": 0.00, "aqu-sz": 0.00, "util": 0.15},
                            {"disk_device": "sdb", "r/s": 0.00, "w/s": 0.00, "d/s": 0.00, "rkB/s": 0.01, "wkB/s": 0.00, "dkB/s": 0.00, "rrqm/s": 0.00, "wrqm/s": 0.00, "drqm/s": 0.00, "rrqm": 16.94, "wrqm": 16.67, "drqm": 0.00, "r_await": 3.48, "w_await": 39.90, "d_await": 0.00, "rareq-sz": 30.90, "wareq-sz": 4.80, "dareq-sz": 0.00, "aqu-sz": 0.00, "util": 0.00},
                            {"disk_device": "sdc", "r/s": 0.07, "w/s": 0.50, "d/s": 0.00, "rkB/s": 3.83, "wkB/s": 91.15, "dkB/s": 0.00, "rrqm/s": 0.01, "wrqm/s": 0.63, "drqm/s": 0.00, "rrqm": 7.07, "wrqm": 55.68, "drqm": 0.00, "r_await": 2.07, "w_await": 9.52, "d_await": 0.00, "rareq-sz": 27.77, "wareq-sz": 91.40, "dareq-sz": 0.00, "aqu-sz": 0.00, "util": 0.12}
                        ]
                    },
                    {
                        "avg-cpu":  {"user": 0.25, "nice": 0.00, "system": 0.00, "iowait": 0.00, "steal": 0.00, "idle": 99.75},
                        "disk": [
                            {"disk_device": "sda", "r/s": 0.00, "w/s": 0.00, "d/s": 0.00, "rkB/s": 0.00, "wkB/s": 0.00, "dkB/s": 0.00, "rrqm/s": 0.00, "wrqm/s": 0.00, "drqm/s": 0.00, "rrqm": 0.00, "wrqm": 0.00, "drqm": 0.00, "r_await": 0.00, "w_await": 0.00, "d_await": 0.00, "rareq-sz": 0.00, "wareq-sz": 0.00, "dareq-sz": 0.00, "aqu-sz": 0.00, "util": 0.00},
                            {"disk_device": "sdb", "r/s": 0.00, "w/s": 0.00, "d/s": 0.00, "rkB/s": 0.00, "wkB/s": 0.00, "dkB/s": 0.00, "rrqm/s": 0.00, "wrqm/s": 0.00, "drqm/s": 0.00, "rrqm": 0.00, "wrqm": 0.00, "drqm": 0.00, "r_await": 0.00, "w_await": 0.00, "d_await": 0.00, "rareq-sz": 0.00, "wareq-sz": 0.00, "dareq-sz": 0.00, "aqu-sz": 0.00, "util": 0.00},
                            {"disk_device": "sdc", "r/s": 0.00, "w/s": 0.00, "d/s": 0.00, "rkB/s": 0.00, "wkB/s": 0.00, "dkB/s": 0.00, "rrqm/s": 0.00, "wrqm/s": 0.00, "drqm/s": 0.00, "rrqm": 0.00, "wrqm": 0.00, "drqm": 0.00, "r_await": 0.00, "w_await": 0.00, "d_await": 0.00, "rareq-sz": 0.00, "wareq-sz": 0.00, "dareq-sz": 0.00, "aqu-sz": 0.00, "util": 0.00}
                        ]
                    }
                ]
            }
        ]
    }}
     */


    // http://sebastien.godard.pagesperso-orange.fr
    // json output is available since 11.5.1 and was released in stable release with 11.6.0 at 14.08.2017
    let iostat = match exec("iostat -o JSON -d -x 1 2", &_session)
    {
        Ok(iostat) => { iostat }
        Err(e) => {
            error!("could not execute command: {:?}", e);
            return Err(e);
        }
    };
    debug!("iostat:\n====\n{}====", iostat);
    let parsed = match json::parse(&*iostat) {
        Ok(parsed) => parsed,
        Err(e) => {
            error!("could not parse json: {:?}", e);
            return Err(Box::new(e));
        }
    };

    let node_name = &parsed["sysstat"]["hosts"][0]["nodename"].to_string();
    debug!("nodeName: {:#?}" , node_name);
    //["sysstat.hosts.nodename"];

    let statistics = &parsed["sysstat"]["hosts"][0]["statistics"];

    let mut uptime = IOStat::default();
    let mut moment = IOStat::default();

    if !statistics.is_empty()
    {
        let mut counter = 0;
        for statistic in statistics.members()
        {
            if counter == 0
            {
                // first element, means statistics from startup of machine to now
                uptime.valid = true;
                let global_statistic = statistic;
                let disks = &global_statistic["disk"];
                if !disks.is_empty()
                {
                    debug!("disks: {:#?}" , disks.to_string());
                    /*
                     "disk": [
                            {"disk_device": "sda", "r/s": 0.00, "w/s": 0.00, "d/s": 0.00, "rkB/s": 0.00, "wkB/s": 0.00, "dkB/s": 0.00, "rrqm/s": 0.00, "wrqm/s": 0.00, "drqm/s": 0.00, "rrqm": 0.00, "wrqm": 0.00, "drqm": 0.00, "r_await": 0.00, "w_await": 0.00, "d_await": 0.00, "rareq-sz": 0.00, "wareq-sz": 0.00, "dareq-sz": 0.00, "aqu-sz": 0.00, "util": 0.00},
                            {"disk_device": "sdb", "r/s": 0.00, "w/s": 0.00, "d/s": 0.00, "rkB/s": 0.00, "wkB/s": 0.00, "dkB/s": 0.00, "rrqm/s": 0.00, "wrqm/s": 0.00, "drqm/s": 0.00, "rrqm": 0.00, "wrqm": 0.00, "drqm": 0.00, "r_await": 0.00, "w_await": 0.00, "d_await": 0.00, "rareq-sz": 0.00, "wareq-sz": 0.00, "dareq-sz": 0.00, "aqu-sz": 0.00, "util": 0.00},
                            {"disk_device": "sdc", "r/s": 0.00, "w/s": 0.00, "d/s": 0.00, "rkB/s": 0.00, "wkB/s": 0.00, "dkB/s": 0.00, "rrqm/s": 0.00, "wrqm/s": 0.00, "drqm/s": 0.00, "rrqm": 0.00, "wrqm": 0.00, "drqm": 0.00, "r_await": 0.00, "w_await": 0.00, "d_await": 0.00, "rareq-sz": 0.00, "wareq-sz": 0.00, "dareq-sz": 0.00, "aqu-sz": 0.00, "util": 0.00}
                        ]
                     */
                    for disk in disks.members()
                    {
                        debug!("disk: {:#?}" , disk.to_string());
                        /*
                            {"disk_device": "sda", "r/s": 0.00, "w/s": 0.00, "d/s": 0.00, "rkB/s": 0.00, "wkB/s": 0.00, "dkB/s": 0.00, "rrqm/s": 0.00, "wrqm/s": 0.00, "drqm/s": 0.00, "rrqm": 0.00, "wrqm": 0.00, "drqm": 0.00, "r_await": 0.00, "w_await": 0.00, "d_await": 0.00, "rareq-sz": 0.00, "wareq-sz": 0.00, "dareq-sz": 0.00, "aqu-sz": 0.00, "util": 0.00},
                         */
                        let disk_stat = parse_disk(disk);
                        uptime.disks.push(disk_stat);
                    }
                }
            } else {
                // last element
                let current_statistic = statistic;
                moment.valid = true;
                let disks = &current_statistic["disk"];
                if !disks.is_empty()
                {
                    for disk in disks.members()
                    {
                        let disk_stat = parse_disk(disk);
                        moment.disks.push(disk_stat);
                    }
                }
            }
            counter += 1;
        }
    }
    return Ok(IOStats {
        uptime,
        moment,
    });
}

fn parse_disk(disk: &JsonValue) -> IOStatDisk {
    let mut disk_stat = IOStatDisk::default();
    for (key, value) in disk.entries()
    {
        match key {
            "disk_device" => {
                disk_stat.device = Box::from(value.as_str().unwrap());
            }
            "r/s" => {
                disk_stat.requests_read_completed_per_second = value.as_f32().unwrap();
            }
            "rkB/s" => {
                disk_stat.requests_read_kilobytes_per_second = value.as_f32().unwrap();
            }
            "rrqm/s" => {
                disk_stat.queued_requests_read_completed_per_second = value.as_f32().unwrap();
            }
            "rrqm" =>
                {
                    disk_stat.requests_read_merged_percentage = value.as_f32().unwrap();
                }
            "r_await" => {
                disk_stat.requests_read_time_average_milliseconds = value.as_f32().unwrap();
            }
            "rareq-sz" => {
                disk_stat.requests_read_size_average_kilobyte = value.as_f32().unwrap();
            }
            "w/s" => {
                disk_stat.requests_write_completed_per_second = value.as_f32().unwrap();
            }
            "wkB/s" => {
                disk_stat.requests_write_kilobytes_per_second = value.as_f32().unwrap();
            }
            "wrqm/s" => {
                disk_stat.queued_requests_writed_completed_per_second = value.as_f32().unwrap();
            }
            "wrqm" => {
                disk_stat.requests_write_merged_percentage = value.as_f32().unwrap();
            }
            "w_await" => {
                disk_stat.requests_write_time_average_milliseconds = value.as_f32().unwrap();
            }
            "wareq-sz" => {
                disk_stat.requests_write_size_average_kilobytes = value.as_f32().unwrap();
            }
            "d/s" => {
                disk_stat.requests_discarded_completed_per_second = value.as_f32().unwrap();
            }
            "dkB/s" => {
                disk_stat.requests_discarded_kilobytes_per_second = value.as_f32().unwrap();
            }
            "drqm/s" => {
                disk_stat.queued_requests_discarded_completed_per_second = value.as_f32().unwrap();
            }
            "drqm" => {
                disk_stat.requests_discarded_merged_percentage = value.as_f32().unwrap();
            }
            "d_await" => {
                disk_stat.requests_read_time_average_milliseconds = value.as_f32().unwrap();
            }
            "dareq-sz" => {
                disk_stat.requests_discarded_size_average_kilobytes = value.as_f32().unwrap();
            }
            "aqu-sz" => {
                disk_stat.requests_discarded_queue_length_average = value.as_f32().unwrap();
            }
            "util" => {
                disk_stat.saturation = value.as_f32().unwrap();
            }
            "svctm" => {
              /*
                The average service time (in milliseconds) for I/O requests that were issued to the
                device. Warning! Do not trust this field any more. This field will be removed in a
                future sysstat version.
              */
            }
            _ => {
                warn!("unhandled member: {:#?}:{:#?}", key, value);
                disk_stat.metrics.push(RawMetric { name: key.to_string(), value: value.as_f32().unwrap() });
            }
        }
    }
    disk_stat
}
