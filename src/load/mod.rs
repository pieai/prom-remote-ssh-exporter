use log::{error};
use crate::ssh::exec;
use ssh2::Session;

#[derive(Debug, Default)]
pub struct LoadAverage {
    pub load1: f32,
    pub load5: f32,
    pub load15: f32,
}

pub fn loadavg(_session: &Session) -> LoadAverage
{
    let loadavg = match exec("cat /proc/loadavg", &_session)
    {
        Err(e) => {
            error!("could not check loadavg: {:?}", e);
            return LoadAverage {
                load1: -1.0,
                load5: -1.0,
                load15: -1.0,
            };
        }
        Ok(o) => o
    };
    //println!("loadavg:\n====\n{}====", loadavg);

    // Wert 1: Anzahl der Prozesse im Status R (lauffähig / runnable) oder D (auf I/O wartend / disk sleep) in der Run Queue als Durchschnitt über 1 Minute
    // Wert 2: Analog zu Wert 1, allerdings als Durchschnitt über 5 Minuten
    // Wert 3: Analog zu Wert 1, allerdings als Durchschnitt über 15 Minuten
    // Wert 4: Der Feld vor dem Schrägstrich enthält die aktuell lauffähigen (runnable) Prozesse / Threads (Kernel Scheduling Entities).
    // Das Feld danach enthält die Anzahl der Kernel Scheduling Entities im System.
    // Wert 5: PID des jüngsten im System erzeugten Prozesses
    let mut parts = loadavg.split_whitespace();
    let one = parts.next().unwrap();
    let five = parts.next().unwrap();
    let fifteen = parts.next().unwrap();

    let res = LoadAverage {
        load1: one.parse::<f32>().unwrap(),
        load5: five.parse::<f32>().unwrap(),
        load15: fifteen.parse::<f32>().unwrap(),
    };

    return res;
}

#[derive(Debug, Default)]
pub struct ThreadsAndCores {
    pub threads: f32,
    pub cores: f32,
}

pub fn cpuinfo(_session: &Session) -> ThreadsAndCores
{
    let loadavg = match exec("cat /proc/cpuinfo", &_session)
    {
        Err(e) => {
            error!("could not check cpuinfo: {:?}", e);
            return ThreadsAndCores {
                threads: -1.0,
                cores: -1.0,
            };
        }
        Ok(o) => o
    };

    let mut threads: f32 = 0.0;
    let cores: f32 = 0.0;

    for l in loadavg.lines()
    {
        let line = l.to_string();
        if line.starts_with("processor")
        {
            threads = threads + 1.0;
        } else if line.starts_with("cpu cores")
        {
            let label_value = line.split(&[':'][..]);
            //TODO implement getting value of line as cores count
        }
    }

    return ThreadsAndCores {
        threads,
        cores,
    };
}